﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.PurchaseRequestModule.DB
{
    public class ConnectionDB
    {
        /// <summary>
        /// Determina se la cadena es local
        /// </summary>
        public string LocalEnvionment = "Local";
        /// <summary>
        /// Determina si la cadena es del servidor
        /// </summary>
        public string ServerEnvionment = "Server";

        /// <summary>
        /// Metodo que retorna la cadena de conexión dependiendo del ambiente
        /// </summary>
        /// <param name="envionment"></param>
        /// <returns></returns>
        public string GetConnectionLocalString(string envionment)
        {
            if (envionment.Equals(this.LocalEnvionment))
            {
                return "Data Source=LAPTOP-1FIV11B1\\SQLEXPRESS2014;Initial Catalog=dnndev;User ID=sa;Password=tasisoft01+";
            }
            return "Data Source=198.12.158.210\\SQLWKG2008R2;Initial Catalog=Tasinet2;User ID=sa;Password=tasisoft01+";
        }
    }
}