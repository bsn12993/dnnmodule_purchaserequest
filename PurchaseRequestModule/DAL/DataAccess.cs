﻿using Christoc.Modules.PurchaseRequestModule.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.PurchaseRequestModule.DAL
{
    public class DataAccess
    {
        private string connectionString { get; set; }
        private ConnectionDB ConnectionDB = new ConnectionDB();

        public DataAccess()
        {
            connectionString = ConnectionDB.GetConnectionLocalString(ConnectionDB.LocalEnvionment);
        }
    }
}