﻿using DotNetNuke.Web.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Christoc.Modules.PurchaseRequestModule.WebApi
{
    public class PurchaseRequestApiController : DnnApiController
    {
        protected PurchaseRequestApiController()
        {
        }

        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage GetMessage()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Hello", "application/json");
        }
    }
}