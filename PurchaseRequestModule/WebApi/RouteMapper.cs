﻿using DotNetNuke.Web.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.PurchaseRequestModule.WebApi
{
    public class RouteMapper : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapHttpRoute("PurchaseRequestModule", "default", "{controller}/{action}",
                new[] { "Christoc.Modules.PurchaseRequestModule.WebApi" });
        }
    }
}