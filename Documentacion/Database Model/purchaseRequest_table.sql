
CREATE TABLE PurchaseRequest
( 
	applicant            varchar(50)  NULL ,
	requestDate          datetime  NULL ,
	service              varchar(100)  NULL ,
	serviceCharacteristics varchar(max)  NULL ,
	proveedorCharacteristics varchar(max)  NULL ,
	amount               int  NULL ,
	requiredDate         datetime  NULL ,
	receptionDate        datetime  NULL ,
	requestStatus        varchar(50)  NULL ,
	authorizationDate    datetime  NULL ,
	assignationType      varchar(50)  NULL ,
	assignedServiceData  varchar(max)  NULL ,
	proveedor            varchar(40)  NULL ,
	price                money  NULL ,
	totalPrice           money  NULL ,
	generalConditions    varchar(max)  NULL ,
	deliveryDate         datetime  NULL ,
	id_purchaseRequest   int IDENTITY ( 1,1 ) 
)
go



ALTER TABLE PurchaseRequest
	ADD CONSTRAINT XPKPurchaseRequest PRIMARY KEY  NONCLUSTERED (id_purchaseRequest ASC)
go


